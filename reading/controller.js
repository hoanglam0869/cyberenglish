import { $ } from "../function/function.js";

let isEnglish = true;

export const renderReading = (title, readingArr) => {
  /* document.querySelector(".reading-content").innerHTML = `
    <p class="title">${title}</p>
    <div class="reading-txt">
        ${readingArr
          .map(
            (reading) => `
            <div class="paragraph-container">
                <button class="ButtonTranslate-btn-translate" style="background-image: url(../img/VnFlag.png);"></button>
                <div class="paragraph"><p>${reading.sentence_en}</p></div>
                <div class="paragraph paragraph-showOff-txt-translate"></div>
            </div>`
          )
          .join("")}
    </div>
  `; */
  const content = document.querySelector(".reading-content");
  $("p", "title", content).text(title);
  const readingTxt = $("div", "reading-txt", content);
  readingArr.forEach((reading) => {
    const paragraphCon = $("div", "paragraph-container", readingTxt);
    const btn = $("button", "ButtonTranslate-btn-translate", paragraphCon);
    btn.style = `background-image: url(../img/VnFlag.png);`;
    const enParagraph = $("div", "paragraph", paragraphCon);
    $("p", "", enParagraph).html(reading.sentence_en);
    const viParagraph = $(
      "div",
      "paragraph paragraph-showOff-txt-translate",
      paragraphCon
    );
    // show or hide Vietnamese
    btn.onclick = () => {
      let isVnFlag = btn.getAttribute("style").includes("VnFlag");
      let style = isVnFlag ? "englishFlag" : "VnFlag";
      btn.style = `background-image: url(../img/${style}.png);`;

      viParagraph.classList.toggle("paragraph-showOff-txt-translate");
      viParagraph.classList.toggle("paragraph-show-txt-translate");
      viParagraph.html(isVnFlag ? reading.sentence_vi : "");
    };
  });
  // modify a tags
  const aTags = readingTxt.querySelectorAll("a");
  aTags.forEach((aTag) => {
    aTag.className = "lightbox-modal";
    aTag.setAttribute("onclick", "window.handleClick(this)");
  });
};

export const renderModal = (vocab) => {
  /* document.body.innerHTML += `
    <div class="portal-container modal-portal">
      <div class="modal-backDrop modal-active">
        <div class="modal-content">
          <div class="reading-container-modal">
            <div class="reading-btnTranslate">
              <button class="ButtonTranslate-btn-translate"></button>
            </div>
            <p class="reading-vocabularyModal"></p>
            <p class="reading-definitionModal"></p>
            <p class="reading-exampleModal"></p>
          </div>
        </div>
      </div>
    </div>`; */
  const portal = $("div", "portal-container modal-portal", document.body);
  const backDrop = $("div", "modal-backDrop modal-active", portal);
  const content = $("div", "modal-content", backDrop);
  const readingCon = $("div", "reading-container-modal", content);
  const btnTranslate = $("div", "reading-btnTranslate", readingCon);
  const btn = $("button", "ButtonTranslate-btn-translate", btnTranslate);
  const vocabulary = $("p", "reading-vocabularyModal", readingCon);
  const def = $("p", "reading-definitionModal", readingCon);
  const example = $("p", "reading-exampleModal", readingCon);

  const setModalContent = () => {
    let style = isEnglish ? "VnFlag" : "englishFlag";
    btn.style = `background-image: url(../img/${style}.png);`;

    if (vocab == undefined) return;
    vocabulary.text(isEnglish ? vocab.word : vocab.translation);
    def.text(isEnglish ? vocab.def_en : vocab.def_vi);
    example.text(isEnglish ? vocab.example_en : vocab.example_vi);
  };
  setModalContent();
  // switch to English or Vietnamese
  btn.onclick = () => {
    isEnglish = !isEnglish;
    setModalContent();
  };
  // click outside to hide modal
  backDrop.onclick = (e) => {
    if (!content.contains(e.target)) portal.remove();
  };
};

export const renderQuestion = (id, questionArr) => {
  /* document.querySelector(".reading-question-container").innerHTML = questionArr
    .map(
      (question) => `
    <div>
      <div>${question.question}</div>
      <div>
        <div class="modern-radio-container">
          <div class="radio-outer-circle unselected">
            <div class="radio-inner-circle unselected-circle"></div>
          </div>
          <div class="helper-text">True</div>
        </div>
        <div class="modern-radio-container">
          <div class="radio-outer-circle unselected">
            <div class="radio-inner-circle unselected-circle"></div>
          </div>
          <div class="helper-text">False</div>
        </div>
      </div>
    </div>
  `
    )
    .join(""); */
  const container = document.querySelector(".reading-question-container");
  let btnQuestion = document.querySelector(".reading-btnQuestion");
  const centerPos = document.querySelector(".reading-centerPosition");
  const div1 = $("div", "", container);

  let radioConArr = [];
  let answers = [];
  questionArr.forEach((question, i) => {
    const div2 = $("div", "", div1);
    $("div", "", div2).text(question.question);
    const div3 = $("div", "", div2);

    let radioOuterArr = [];
    let radioInnerArr = [];
    ["True", "False"].forEach((text, j) => {
      let radioCon = $("div", "modern-radio-container", div3);
      radioOuterArr[j] = $("div", "radio-outer-circle unselected", radioCon);
      radioInnerArr[j] = $(
        "div",
        "radio-inner-circle unselected-circle",
        radioOuterArr[j]
      );
      $("div", "helper-text", radioCon).text(text);

      radioCon.onclick = () => {
        radioOuterArr[j].className = "radio-outer-circle false";
        radioInnerArr[j].className = "radio-inner-circle false";

        let index = j == 0 ? 1 : 0;
        radioOuterArr[index].className = "radio-outer-circle unselected";
        radioInnerArr[index].className = "radio-inner-circle unselected-circle";

        radioConArr[i] = radioCon;
        answers[i] = radioCon.text().toLowerCase();

        if (radioConArr.length == questionArr.length) {
          btnQuestion.removeAttribute("disabled");
        }
      };
    });
  });
  btnQuestion.onclick = () => {
    questionArr.forEach((question, i) => {
      let correctAnswer = question.answer.toString();
      if (correctAnswer == answers[i]) {
        $("div", "fa fa-check-circle", radioConArr[i]);
      } else {
        $("div", "fa fa-times-circle", radioConArr[i]);
      }
    });
    $("div", "reading-backDrop", div1);
    centerPos.innerHTML = `<a href="/lesson/unit.html?${id}">
      <button class="reading-btnQuestion">QUIT</button>
    </a>`;
    /* const aTag = $("a", "", centerPos);
    aTag.href = `/lesson/unit.html?${id}`;
    $("button", "reading-btnQuestion", aTag).text("QUIT"); */
  };
};
