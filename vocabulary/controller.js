import { $ } from "../function/function.js";
import { localVocabularyStorage } from "../local-storage/local-storage.js";

export const renderVocabulary = (vocabularyArr) => {
  /* document.querySelector(
    ".vocabulary-page-container"
  ).innerHTML = `<div>${vocabularyArr
    .map(
      (vocabulary) => `
    <div class="vocabulary-container">
      <div class="vocabulary-definition-container">
        <div class="vocabulary-btn-container">
          <button class="vocabulary-btn">
            <i data-src-audio="${vocabulary.sound_us}" class="fas fa-volume-up"></i>
            <i class="fa fa-male"></i>
          </button>
          <button class="vocabulary-btn">
            <i data-src-audio="${vocabulary.sound_uk}" class="fas fa-volume-up"></i>
            <i class="fa fa-female"></i>
          </button>
        </div>
        <div class="vocabulary-txt-vocabulary">
          <div class="vocabulary-txt-translate-wrapper">
            <p>${vocabulary.word}</p>
            <p class="vocabulary-line-spelling">-</p>
            <span class="vocabulary-txt-spelling">${vocabulary.pronunciation}</span>
          </div>
          <p class="vocabulary-txt-translate vocabulary-showOff-txt-translate">${vocabulary.translation}</p>
        </div>
        <div class="vocabulary-addList-line">
          <button class="ButtonAdd-btn-add">
            <i class="fa fa-plus"></i>
            <i class="fa fa-list"></i>
          </button>
        </div>
        <div class="vocabulary-txt-definition">
          <p>${vocabulary.def_en}</p>
          <p class="vocabulary-txt-translate vocabulary-showOff-txt-translate">${vocabulary.def_vi}</p>
        </div>
        <div class="vocabulary-btn-translate">
          <button class="ButtonTranslate-btn-translate" style="background-image: url(../img/VnFlag.png);"></button>
        </div>
      </div>
      <div class="vocabulary-example-container">
        <div class="vocabulary-btn-container">
          <button class="vocabulary-btn">
            <i data-src-audio="${vocabulary.example_us}" class="fas fa-volume-up"></i>
            <i class="fa fa-male"></i>
          </button>
          <button class="vocabulary-btn">
            <i data-src-audio="${vocabulary.example_uk}" class="fas fa-volume-up"></i>
            <i class="fa fa-female"></i>
          </button>
        </div>
        <div class="vocabulary-txt-example">
          <p>${vocabulary.example_en}</p>
          <p class="vocabulary-txt-translate vocabulary-showOff-txt-translate">${vocabulary.example_vi}</p>
        </div>
      </div>
    </div>
  `
    )
    .join("")}</div>`; */
  const createAudioEl = (container, url) => {
    let btn = $("button", "vocabulary-btn", container);
    let sound = $("i", "fas fa-volume-up", btn);
    sound.setAttribute("data-src-audio", url);
    $("i", "fa fa-female", btn);
    return btn;
  };
  const playSound = (url) => {
    let audio = new Audio(url);
    audio.preload;
    audio.play();
  };
  const pageCon = document.querySelector(".vocabulary-page-container");
  const div = $("div", "", pageCon);
  vocabularyArr.forEach((vocab) => {
    const con = $("div", "vocabulary-container", div);

    const defCon = $("div", "vocabulary-definition-container", con);
    const btnCon1 = $("div", "vocabulary-btn-container", defCon);
    // sound US
    let btn1 = createAudioEl(btnCon1, vocab.sound_us);
    btn1.onclick = () => playSound(vocab.sound_us);
    // sound UK
    let btn2 = createAudioEl(btnCon1, vocab.sound_uk);
    btn2.onclick = () => playSound(vocab.sound_uk);
    // word, pronunciation, translation
    const txtVocab = $("div", "vocabulary-txt-vocabulary", defCon);
    const wrapper = $("div", "vocabulary-txt-translate-wrapper", txtVocab);
    $("p", "", wrapper).text(vocab.word);
    $("p", "vocabulary-line-spelling", wrapper).text("-");
    $("span", "vocabulary-txt-spelling", wrapper).text(vocab.pronunciation);
    const trans = $(
      "p",
      "vocabulary-txt-translate vocabulary-showOff-txt-translate",
      txtVocab
    ).text(vocab.translation);
    // add word to list
    const addList = $("div", "vocabulary-addList-line", defCon);
    const btnAdd = $("button", "ButtonAdd-btn-add", addList);
    $("i", "fa fa-plus", btnAdd);
    $("i", "fa fa-list", btnAdd);
    btnAdd.onclick = () => localVocabularyStorage.set(vocab);
    // definition
    const txtDef = $("div", "vocabulary-txt-definition", defCon);
    $("p", "", txtDef).text(vocab.def_en);
    const defVi = $(
      "p",
      "vocabulary-txt-translate vocabulary-showOff-txt-translate",
      txtDef
    ).text(vocab.def_vi);
    // show/hide Vietnamese button
    const btnTranslate = $("div", "vocabulary-btn-translate", defCon);
    const btn = $("button", "ButtonTranslate-btn-translate", btnTranslate);
    btn.style = `background-image: url(../img/VnFlag.png)`;
    btn.onclick = () => {
      let isVnFlag = btn.getAttribute("style").includes("VnFlag");
      let style = isVnFlag ? "englishFlag" : "VnFlag";
      btn.style = `background-image: url(../img/${style}.png)`;

      let clN = `vocabulary-show${isVnFlag ? "" : "Off"}-txt-translate`;
      trans.className = `vocabulary-txt-translate ${clN}`;
      defVi.className = `vocabulary-txt-translate ${clN}`;
      exVi.className = `vocabulary-txt-translate ${clN}`;
    };

    const exCon = $("div", "vocabulary-example-container", con);
    const btnCon2 = $("div", "vocabulary-btn-container", exCon);
    // sound US
    let btn3 = createAudioEl(btnCon2, vocab.example_us);
    btn3.onclick = () => playSound(vocab.example_us);
    // sound UK
    let btn4 = createAudioEl(btnCon2, vocab.example_uk);
    btn4.onclick = () => playSound(vocab.example_uk);
    // example
    const txtEx = $("div", "vocabulary-txt-example", exCon);
    $("p", "", txtEx).text(vocab.example_en);
    const exVi = $(
      "p",
      "vocabulary-txt-translate vocabulary-showOff-txt-translate",
      txtEx
    ).text(vocab.example_vi);
  });
};
