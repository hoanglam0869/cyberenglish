import {
  getLessonsId,
  hideLoading,
  showLoading,
} from "../function/function.js";
import { courses } from "../lesson/courses.js";
import { renderNavigation } from "../nav/controller.js";
import { renderVocabulary } from "./controller.js";

const id = getLessonsId();

renderNavigation(id, courses, 0);

let vocabularyArr = [];

const fetchData = async () => {
  showLoading();
  let res = await fetch("./utils/json/vocabulary.json");
  vocabularyArr = await res.json();
  vocabularyArr = vocabularyArr.filter((vocab) => vocab.lesson_id == id);
  renderVocabulary(vocabularyArr);
  hideLoading();
};

fetchData();
