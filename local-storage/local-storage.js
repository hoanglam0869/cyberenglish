const VOCABULARY = "VOCABULARY";
const LISTENING = "LISTENING";
const MULTIPLE_CHOICE = "MULTIPLE_CHOICE";

export const localVocabularyStorage = {
  get: (search = "") => {
    let jsonData = localStorage.getItem(VOCABULARY);
    let json = JSON.parse(jsonData);
    if (json != null) {
      json = json.filter((vocabulary) => {
        let word = vocabulary.word.toLowerCase();
        let translation = vocabulary.translation.toLowerCase();
        if (word.indexOf(search) != -1 || translation.indexOf(search) != -1) {
          return true;
        }
      });
      return json;
    }
    return [];
  },
  set: (vocabulary) => {
    let vocabularyArr = localVocabularyStorage.get();
    let index = vocabularyArr.findIndex(
      (item) =>
        item.lesson_id == vocabulary.lesson_id && item.order == vocabulary.order
    );
    if (index == -1) {
      vocabulary.highlight = false;
      vocabularyArr.push(vocabulary);
      let jsonData = JSON.stringify(vocabularyArr);
      localStorage.setItem(VOCABULARY, jsonData);
      showToast("Successfully added to word list!");
    }
  },
  highlight: (vocabulary) => {
    let vocabularyArr = localVocabularyStorage.get();
    let index = vocabularyArr.findIndex(
      (item) =>
        item.lesson_id == vocabulary.lesson_id && item.order == vocabulary.order
    );
    if (index != -1) {
      vocabularyArr[index].highlight = !vocabulary.highlight;
      let jsonData = JSON.stringify(vocabularyArr);
      localStorage.setItem(VOCABULARY, jsonData);
    }
  },
  remove: (vocabulary) => {
    let vocabularyArr = localVocabularyStorage.get();
    vocabularyArr = vocabularyArr.filter(
      (item) =>
        item.lesson_id != vocabulary.lesson_id || item.order != vocabulary.order
    );
    let jsonData = JSON.stringify(vocabularyArr);
    localStorage.setItem(VOCABULARY, jsonData);
  },
};

export const localListeningStorage = {
  getAll: () => {
    let jsonData = localStorage.getItem(LISTENING);
    let json = JSON.parse(jsonData);
    return json == null ? [] : json;
  },
  get: (id) => {
    let listeningArr = localListeningStorage.getAll();
    if (listeningArr.length == 0) {
      return { lesson_id: id, index: 0, correct: 0, total: 0, lessons: [] };
    } else {
      let index = listeningArr.findIndex((item) => item.lesson_id == id);
      if (index == -1) {
        return { lesson_id: id, index: 0, correct: 0, total: 0, lessons: [] };
      } else {
        return listeningArr[index];
      }
    }
  },
  set: (listening) => {
    let listeningArr = localListeningStorage.getAll();
    let index = listeningArr.findIndex(
      (item) => item.lesson_id == listening.lesson_id
    );
    if (index == -1) {
      listeningArr.push(listening);
    } else {
      listeningArr[index] = listening;
    }
    let jsonData = JSON.stringify(listeningArr);
    localStorage.setItem(LISTENING, jsonData);
  },
  reset: (id) => {
    let listeningArr = localListeningStorage.getAll();
    listeningArr = listeningArr.filter((item) => item.lesson_id != id);
    let jsonData = JSON.stringify(listeningArr);
    localStorage.setItem(LISTENING, jsonData);
  },
};

export const localMultipleChoiceStorage = {
  getAll: () => {
    let jsonData = localStorage.getItem(MULTIPLE_CHOICE);
    let json = JSON.parse(jsonData);
    return json == null ? [] : json;
  },
  get: (id) => {
    let multipleChoiceArr = localMultipleChoiceStorage.getAll();
    if (multipleChoiceArr.length == 0) {
      return { lesson_id: id, index: 0, correct: 0, total: 0, lessons: [] };
    } else {
      let index = multipleChoiceArr.findIndex((item) => item.lesson_id == id);
      if (index == -1) {
        return { lesson_id: id, index: 0, correct: 0, total: 0, lessons: [] };
      } else {
        return multipleChoiceArr[index];
      }
    }
  },
  set: (multipleChoice) => {
    let multipleChoiceArr = localMultipleChoiceStorage.getAll();
    let index = multipleChoiceArr.findIndex(
      (item) => item.lesson_id == multipleChoice.lesson_id
    );
    if (index == -1) {
      multipleChoiceArr.push(multipleChoice);
    } else {
      multipleChoiceArr[index] = multipleChoice;
    }
    let jsonData = JSON.stringify(multipleChoiceArr);
    localStorage.setItem(MULTIPLE_CHOICE, jsonData);
  },
  reset: (id) => {
    let multipleChoiceArr = localMultipleChoiceStorage.getAll();
    multipleChoiceArr = multipleChoiceArr.filter(
      (item) => item.lesson_id != id
    );
    let jsonData = JSON.stringify(multipleChoiceArr);
    localStorage.setItem(MULTIPLE_CHOICE, jsonData);
  },
};

const showToast = (msg) => {
  Toastify({
    text: msg,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "bottom", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
