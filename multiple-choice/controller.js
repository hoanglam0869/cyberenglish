import { $ } from "../function/function.js";

const radioCon = document.querySelectorAll(".modern-radio-container");
const radOut = document.querySelectorAll(".radio-outer-circle");
const radIn = document.querySelectorAll(".radio-inner-circle");
const helperTexts = document.querySelectorAll(".helper-text");
const overlay = document.querySelector(".overlay");
const notification = document.querySelector(".show-notification");

let chosen = "";

function isAnswer() {
  let rnd = Math.floor(Math.random() * 2);
  return rnd == 1 ? true : false;
}

const clearRadio = () => {
  radOut.forEach((o) => (o.className = "radio-outer-circle unselected"));
  radIn.forEach((i) => (i.className = "radio-inner-circle unselected-circle"));
};

export const renderAnswers = (vocabularyArr, currentIndex) => {
  overlay.style.display = "none";
  chosen = "";
  clearRadio();
  let answerArr = [];
  for (let i = 0; i < vocabularyArr.length; i++) {
    const def = vocabularyArr[i].def_en;
    if (def != vocabularyArr[currentIndex].def_en) {
      if (isAnswer() || 2 - answerArr.length == vocabularyArr.length - i) {
        answerArr.push(def);
      }
    }
    if (answerArr.length == 2) break;
  }
  answerArr.push(vocabularyArr[currentIndex].def_en);
  answerArr.sort(() => Math.random() - 0.5);
  helperTexts.forEach((helperText, i) => (helperText.innerText = answerArr[i]));

  radioCon.forEach((radio, i) => {
    radio.onclick = () => {
      clearRadio();

      radOut[i].className = "radio-outer-circle false";
      radIn[i].className = "radio-inner-circle false";

      chosen = radio.innerText;
    };
  });
};

export const renderNotification = (id, def, event) => {
  notification.innerHTML = "";
  const con = $("div", "choice-btn-container", notification);
  const btn = $("button", "btn undefined", con).text("SUBMIT");
  const aTag = $("a", "", con);
  aTag.href = `/lesson/unit.html?${id}`;
  aTag.style = "color: rgb(0, 0, 0); text-decoration: none";
  $("button", "btn undefined", aTag).text("QUIT");

  btn.onclick = () => {
    if (chosen == "") return;
    overlay.style.display = "block";
    notification.innerHTML = "";
    let isTrue = def == chosen;
    if (isTrue) {
      event.onSuccess();
    } else {
      event.onFail();
    }
    let clName = isTrue ? "success" : "fail";
    const result = $("div", `choice-btn-container-${clName}`, notification);
    const status = $("div", `${isTrue ? "excellent" : "incorrect"}`, result);
    $("i", `fa fa-${isTrue ? "check" : "times"}`, status);
    $("span", "", status).text(`${isTrue ? "Excellent!" : "Incorrect!"}`);
    const nextBtn = $("button", "btn next", result).text("NEXT");
    nextBtn.onclick = () => event.onNext();
  };
};
