import {
  getLessonsId,
  hideLoading,
  showLoading,
} from "../function/function.js";
import { courses } from "../lesson/courses.js";
import { localMultipleChoiceStorage } from "../local-storage/local-storage.js";
import { renderNavigation } from "../nav/controller.js";
import { renderAnswers, renderNotification } from "./controller.js";

const id = getLessonsId();

renderNavigation(id, courses, 3);

const restart = document.querySelector(".btn-restart");
const status = document.querySelectorAll(".showStatic p");
const progress = document.querySelector(".progress-line-correct");
const question = document.querySelector(".question");

let multipleChoice = {};
let vocabularyArr = [];
let currentIndex = 0;

let correctAudio = new Audio("./audio/duolingo-correct.mp3");
correctAudio.preload;
let wrongAudio = new Audio("./audio/duolingo-wrong.mp3");
wrongAudio.preload;

const length = () => vocabularyArr.length;
let event = {
  onSuccess: () => {
    correctAudio.play();

    currentIndex++;
    multipleChoice.index = multipleChoice.index + 1;
    multipleChoice.correct = multipleChoice.correct + 1;

    multipleChoice.total = multipleChoice.total + 1;
    localMultipleChoiceStorage.set(multipleChoice);
  },
  onFail: () => {
    wrongAudio.play();

    let temp = vocabularyArr[currentIndex];
    vocabularyArr.splice(currentIndex, 1);
    vocabularyArr.push(temp);

    multipleChoice.total = multipleChoice.total + 1;
    localMultipleChoiceStorage.set(multipleChoice);
  },
  onNext: () => {
    if (currentIndex == vocabularyArr.length) {
      reset();
    } else {
      setupMultipleChoice();
    }
  },
};

const fetchData = async () => {
  showLoading();
  multipleChoice = localMultipleChoiceStorage.get(id);
  if (multipleChoice.lessons.length == 0) {
    let res = await fetch("./utils/json/vocabulary.json");
    vocabularyArr = await res.json();
    vocabularyArr = vocabularyArr.filter((vocab) => vocab.lesson_id == id);
    reset();
  } else {
    vocabularyArr = multipleChoice.lessons;
    currentIndex = multipleChoice.index % length();
    setupMultipleChoice();
  }
  hideLoading();
};

fetchData();

const reset = () => {
  vocabularyArr.sort(() => Math.random() - 0.5);
  multipleChoice.lessons = vocabularyArr;
  // lưu lessons mới vào localStorage, để khi load trang không render lại lessons cũ
  localMultipleChoiceStorage.set(multipleChoice);
  currentIndex = 0;
  setupMultipleChoice();
};

function setupMultipleChoice() {
  // status
  let index = multipleChoice.index > length() ? length() : multipleChoice.index;
  status[0].innerText = `${index} of ${length()}`;
  let correct = 0;
  if (multipleChoice.total != 0) {
    correct = multipleChoice.correct / multipleChoice.total;
  }
  status[1].innerText = `${Math.floor(correct * 100)}% correct so far`;
  // progress bar
  progress.style = `width: ${(index * 100) / length()}%;`;
  // question
  question.innerText = vocabularyArr[currentIndex].word;
  // answers
  renderAnswers(vocabularyArr, currentIndex);
  // show notification
  renderNotification(id, vocabularyArr[currentIndex].def_en, event);
}

restart.onclick = () => {
  restart.classList.add("success");
  localMultipleChoiceStorage.reset(id);
  setTimeout(() => {
    restart.classList.remove("success");
    // lấy object listening sau khi reset
    multipleChoice = localMultipleChoiceStorage.get(id);
    reset();
  }, 1000);
};

document.querySelectorAll("p")[0].onclick = () => {
  const radioCon = document.querySelectorAll(".modern-radio-container");
  for (let i = 0; i < radioCon.length; i++) {
    if (radioCon[i].innerText == vocabularyArr[currentIndex].def_en) {
      radioCon[i].click();
      break;
    }
  }
  setTimeout(() => {
    document.querySelector(".btn.undefined").click();
    setTimeout(() => {
      document.querySelector(".btn.next").click();
    }, 1000);
  }, 1000);
};
