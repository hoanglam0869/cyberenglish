import { $ } from "../function/function.js";

const getRndInteger = (min, max) =>
  Math.floor(Math.random() * (max - min + 1)) + min;

export const renderLessons = (lessonsArr) => {
  /* document.getElementById("lessons-page").innerHTML = lessonsArr
    .map(
      (lesson) => `
        <a href="/lesson/unit.html?${
          lesson.id
        }" style="text-decoration: none; ${
        lesson.lock ? "pointer-events: none" : "cursor: pointer"
      };">
            <div style="position: relative; transform: rotate(${getRndInteger(
              -2,
              3
            )}deg);">
                <div class="item">
                    <i class="fa fa-lock lock-icon"${
                      lesson.lock ? "" : ` style="display: none;"`
                    }></i>
                    <svg class="sc-7v08c9-9 itVPGs" width="196" height="58" viewBox="0 0 196 58" xmlns="http://www.w3.org/2000/svg">
                        <g id="Onboarding" fill="none" fill-rule="evenodd"><path d="M0 7.316c29.064-5.713 57.327-8.08 84.788-7.1C122.55 1.561 159.621 7.547 196 18.172v39H0V7.316z" id="Rectangle" fill="#${
                          lesson.lock ? "e5e5e5" : "FF9D6F"
                        }"></path></g>
                    </svg>
                    <span>${lesson.name}</span>
                    <p>Lesson ${lesson.id}</p>
                </div>
            </div>
        </a>
    `
    )
    .join(""); */
  const page = document.getElementById("lessons-page");
  lessonsArr.forEach((lesson) => {
    const aTag = $("a", "", page);
    aTag.href = `/lesson/unit.html?${lesson.id}`;
    let style = lesson.lock ? "pointer-events: none" : "cursor: pointer";
    aTag.style = `text-decoration: none; ${style};`;

    const div1 = $("div", "", aTag);
    let deg = getRndInteger(-2, 3);
    div1.style = `position: relative; transform: rotate(${deg}deg);`;

    const item = $("div", "item", div1);
    const iTag = $("i", `fa fa-lock lock-icon`, item);
    if (!lesson.lock) iTag.style = "display: none";

    item.innerHTML += `<svg class="sc-7v08c9-9 itVPGs" width="196" height="58" viewBox="0 0 196 58" xmlns="http://www.w3.org/2000/svg">
      <g id="Onboarding" fill="none" fill-rule="evenodd"><path d="M0 7.316c29.064-5.713 57.327-8.08 84.788-7.1C122.55 1.561 159.621 7.547 196 18.172v39H0V7.316z" id="Rectangle" fill="#${
        lesson.lock ? "e5e5e5" : "FF9D6F"
      }"></path></g>
    </svg>`;

    $("span", "", item).text(lesson.name);
    $("p", "", item).text(`Lesson ${lesson.id}`);
  });
};
