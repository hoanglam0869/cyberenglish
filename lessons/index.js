import { hideLoading, showLoading } from "../function/function.js";
import { renderLessons } from "./controller.js";

let lessonsArr = [];

const fetchData = async () => {
  showLoading();
  let res = await fetch("./utils/json/lessons.json");
  lessonsArr = await res.json();
  renderLessons(lessonsArr);
  hideLoading();
};

fetchData();
