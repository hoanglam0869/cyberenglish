import { $ } from "../function/function.js";

export const renderLesson = (id, courses) => {
  /* document.querySelector(".lesson").innerHTML += courses
    .map(
      (course, index) => `
    <div class="course-item"${
      index % 2 == 0 ? ` style="transform: translateY(-10%)"` : ""
    }>
        <a href="/${course.name.replace(
          " ",
          ""
        )}.html?${id}" style="color: rgb(251, 179, 0);">
            <div class="course-item-front">
                <div class="inner">
                    <h1>${course.title}</h1>
                    <h4>${course.en_desc}</h4>
                </div>
            </div>
            <div class="course-item-back">
                <div class="inner" style="text-align: center;">
                    <h4>${course.vi_desc}</h4>
                </div>
            </div>
        </a>
    </div>
`
    )
    .join(""); */
  const lesson = document.querySelector(".lesson");
  courses.forEach((course, i) => {
    const item = $("div", "course-item", lesson);
    if (i % 2 == 0) item.style = "transform: translateY(-10%);";

    const aTag = $("a", "", item);
    aTag.href = `/${course.name.replace(" ", "")}.html?${id}`;
    aTag.style = "color: rgb(251, 179, 0);";

    const itemFront = $("div", "course-item-front", aTag);
    const inner1 = $("div", "inner", itemFront);
    $("h1", "", inner1).html(course.title);
    $("h4", "", inner1).html(course.en_desc);

    const itemBack = $("div", "course-item-back", aTag);
    const inner2 = $("div", "inner", itemBack);
    $("h4", "", inner2).text(course.vi_desc);
  });
};
