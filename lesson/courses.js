export const courses = [
  {
    name: "vocabulary",
    title: "Vocabulary<br>Practice",
    en_desc: "Practice your vocabulary<br>and explore the language",
    vi_desc: "Học từ vựng một cách dễ dàng!",
  },
  {
    name: "reading",
    title: "Reading<br>Practice",
    en_desc: "Practice your reading<br>and explore the language",
    vi_desc:
      "Cải thiện kĩ năng Reading, giúp bạn học thuộc từ vựng ở phần Vocabulary!",
  },
  {
    name: "listening",
    title: "Listening<br>Practice",
    en_desc: "Practice your Listening<br>and explore the language",
    vi_desc: "Cải thiện kĩ năng Listening bằng cách nghe và ghi lại đáp án!",
  },
  {
    name: "multiple Choice",
    title: "Multiple Choice<br>Practice",
    en_desc: "Practice your Multiple Choice<br>and explore the language",
    vi_desc: "Luyện tập thông qua danh sách câu hỏi trắc nghiệm!",
  },
];
