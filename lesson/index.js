import {
  getLessonsId,
  hideLoading,
  showLoading,
} from "../function/function.js";
import { renderLesson } from "./controller.js";
import { courses } from "./courses.js";

const id = getLessonsId();

showLoading();
renderLesson(id, courses);
hideLoading();
