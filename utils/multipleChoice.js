const vocabularyArr = [];

document.querySelector("p").onclick = () => {
  multipleChoice(0);
};

function multipleChoice(index) {
  const q = document.querySelector(".MultipleChoicePage_question__3YqGS");
  const word = q.innerText;

  const radios = document.querySelectorAll(".modern-radio-container");

  for (let i = 0; i < vocabularyArr.length; i++) {
    const vocab = vocabularyArr[i];
    let isCorrect = false;

    if (vocab.word == word) {
      for (let j = 0; j < radios.length; j++) {
        if (radios[j].innerText.toLowerCase() == vocab.def_en.toLowerCase()) {
          radios[j].click();
          isCorrect = true;
          break;
        }
      }
      if (isCorrect) break;
    }
  }

  document.querySelector(".MultipleChoicePage_btn__1GWbx.undefined").click();
  setTimeout(() => {
    const clN = ".MultipleChoicePage_btn__1GWbx.MultipleChoicePage_next__2yvIh";
    document.querySelector(clN).click();
    setTimeout(() => {
      if (index <= vocabularyArr.length) multipleChoice(++index);
    }, 2000);
  }, 2000);
}
