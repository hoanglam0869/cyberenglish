// Reading
const containers = document.getElementsByClassName(
  "Paragraph_paragraph_container__1GuJG"
);

for (let i = 0; i < containers.length; i++) {
  const sentences = containers[i].getElementsByClassName(
    "Paragraph_paragraph__ThmR9"
  );
  const sentence_en = sentences[0].querySelector("p").innerHTML;
  const sentence_vi = sentences[1].innerHTML;
  console.log(sentence_en + "\t" + sentence_vi);
}

// Question
const container = document.getElementsByClassName(
  "ReadingPage_question_container__p6Bj9"
);
const questions = container[0].innerText.split("\n");
for (let i = 0; i < questions.length / 3; i++) {
  console.log(
    questions[i * 3] + "\t" + questions[i * 3 + 1] + "\t" + questions[i * 3 + 2]
  );
}

// Audio
const audio = document.getElementsByTagName("audio");
const url = audio[0].getAttribute("src");
console.log(url);

fetchFile(url);

function fetchFile(url) {
  fetch(url)
    .then((res) => res.blob())
    .then((file) => {
      let tempUrl = URL.createObjectURL(file);
      let aTag = document.createElement("a");
      aTag.href = tempUrl;
      aTag.download = "reading";
      document.body.appendChild(aTag);
      aTag.click();
      aTag.remove();
      URL.revokeObjectURL(tempUrl);
    })
    .catch(() => {
      alert("Failed to download file!");
    });
}
