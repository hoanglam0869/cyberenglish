const vocabularyArr = [];
const input = document.querySelector("input");

input.onkeyup = (e) => {
  if (e.key == "ArrowUp") {
    const letters = document.querySelectorAll(".Letter_container__Z5ePM");

    for (let i = 0; i < vocabularyArr.length; i++) {
      const vocab = vocabularyArr[i].word;
      if (vocab.length == letters.length) {
        let count = 0;
        letters.forEach((letter, j) => {
          if (letter.innerText == "") {
            count++;
          } else if (letter.innerText.toLowerCase() == vocab[j].toLowerCase()) {
            count++;
          }
        });
        if (count == vocab.length) {
          input.value = vocab;
          input.focus();
          break;
        }
      }
    }
  }
};
