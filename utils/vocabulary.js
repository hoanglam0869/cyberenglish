const containers = document.getElementsByClassName(
  "Vocabulary_container__3wlqE"
);
getData(0);

function getData(i) {
  if (i == containers.length) return;

  const audios = containers[i].getElementsByClassName("fas fa-volume-up");
  const word_us = audios[0].getAttribute("data-src-audio");
  const word_uk = audios[1].getAttribute("data-src-audio");
  const example_us = audios[2].getAttribute("data-src-audio");
  const example_uk = audios[3].getAttribute("data-src-audio");

  const pTags = containers[i].getElementsByTagName("p");
  const word = pTags[0].innerText;
  const translation = pTags[2].innerText;
  const definition_en = pTags[3].innerText;
  const definition_vi = pTags[4].innerText;
  const example_en = pTags[5].innerText;
  const example_vi = pTags[6].innerText;

  const spanTags = containers[i].getElementsByTagName("span");
  const spelling = spanTags[0].innerText;

  console.log(
    `${word}\t${spelling}\t${word_us}\t${word_uk}\t${translation}\t${definition_en}\t${definition_vi}\t${example_en}\t${example_vi}\t${example_us}\t${example_uk}`
  );

  fetchFile(word_us, `${word.replace(/ /g, "_").toLowerCase()}_us`);
  setTimeout(function () {
    fetchFile(word_uk, `${word.replace(/ /g, "_").toLowerCase()}_uk`);
    setTimeout(function () {
      fetchFile(
        example_us,
        `${word.replace(/ /g, "_").toLowerCase()}_example_us`
      );
      setTimeout(function () {
        fetchFile(
          example_uk,
          `${word.replace(/ /g, "_").toLowerCase()}_example_uk`
        );
        setTimeout(function () {
          getData(++i);
        }, 1000);
      }, 1000);
    }, 1000);
  }, 1000);
}

function fetchFile(url, name) {
  fetch(url)
    .then((res) => res.blob())
    .then((file) => {
      let tempUrl = URL.createObjectURL(file);
      let aTag = document.createElement("a");
      aTag.href = tempUrl;
      aTag.download = name;
      document.body.appendChild(aTag);
      aTag.click();
      aTag.remove();
      URL.revokeObjectURL(tempUrl);
    })
    .catch(() => {
      alert("Failed to download file!");
    });
}
